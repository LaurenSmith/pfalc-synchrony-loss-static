# Description

This repository contains code for modelling and analyzing populations of coupled and uncoupled phase oscillators with and without replication. In particular, the code supports creating so-called "staging curves" from a population of phase oscillators and a choice of N discrete phases defined by N intervals of phases partitioning [0,1]. The code also supports fitting these models to staging curve data determined by experiment. The code is used to analyze staging curves derived from 5 strains of _Plasmodium_ _falciparum_.

# Dependencies

The code in this repository is written in MATLAB and has been fully tested in version R2019a.

The code requires the MATLAB _Wavelet_ toolbox (https://www.mathworks.com/products/wavelet.html).

Some figures require the MATLAB _boundedline_ toolbox (https://www.mathworks.com/matlabcentral/fileexchange/27485-boundedline-m).

Some analysis requires the MATLAB _extrema_ toolbox (https://www.mathworks.com/matlabcentral/fileexchange/12275-extrema-m-extrema2-m) (included)

# Contents

## src/leise_2012/

Code for analysis of fibroblast PER2::LUC bioluminescence data reported in Leise et al, 2012 (https://doi.org/10.1371/journal.pone.0033334) to estimate the intrinsic variability of the period times of a known, free-running, biological clock without external entrainment stimuli. 

* `leise_2012.m` - Script performing reanalysis of Leise 2012 (https://doi.org/10.1371/journal.pone.0033334) fibroblast PER2::LUC bioluminescence data. Generates results and figures to ensure consistency with the original published work. 
* `compute_p2p_times.m` - Function that finds the peaks (local maxima) of the signal sig, eliminates spurious peaks and returns the peak-to-peak times of the filtered set of peaks. A peak is called spurious if the difference between its value and either the preceeding or succeeding local min is less than thresh. 
* `extrema.m` - Function that returns the global minima and maxima points of the vector ignoring NaN`s. Written by Lic. on Physics Carlos Adrián Vargas Aguilera Physical Oceanography MS candidate UNIVERSIDAD DE GUADALAJARA Mexico, 2004. nubeobscura@hotmail.com

## src/pfalc/

Code for analysis and modelling of Plasmodium falciparum staging curve data gathered from 5 different strains: 3D7, SA250, FVO-NIH, D6, and HB3 which was estimated from Bozdech et al, 2003 (https://doi.org/10.1371/journal.pbio.0000005).

* `create_optimal_params_table.m` - Script that generates the strain-specific optimal parameter and coefficient of variation table, assuming fixed intrinsic cycle lengths, and no replication. It stores the results in a data structure for analysis and plotting
* `create_control_params_table.m` - Script generates the strain-specific control parameters and coefficient of variation table, storing the modelling results in a data structure for analysis and plotting. Here we insist on larger variability in cycle period than is observed by parameter optimization or in the Leise 2012, circadian data. Specifically the minimum value of is taken to be X*p2p_sigma, for X = 1.25, 1.5, 1.75 and where p2p_sigma is the value of the shape parameter for the best fit log-logistic distribution to the Leise 2012 peak-to-peak times
* `generate_plots.m` - Script that generates plots of real and simulated staging curves using optimized parameters and parameters determined from the Leise data. It also creates supplemental figures. Run create_opt_params_table.m first to create and update `../../data/pfalc/opt_params_struct.mat` if it does not already exist
* `generate_tables.m` - Script that generates human-readable, text-format data tables containing results of parameter optimization contained in `../../data/pfalc/opt_params_struct.mat` and `../../data/pfalc/cont_params_struct.mat`
* `default_args.m` - Function constructing a default argument structure containing the experimental data, the various run arguments, and genetic algorithm options needed to specify the model and perform global optimization of model parameters. This ensures that the many arguments are trackable and easy to identify to ensure reproducible results
* `mod_neg.m` - Function that computes mod(x,k) only the on positive entries of x
* `pfalc_idc_data.m` - A helper function to improve ease of accessing the staging curves contained in `data/pfalc/`
* `oscillator_stages.m` - A function that maps an oscillator`s phase to a categorical stage and determines the percentage of the population in each stage at each time
* `pfalc_idc_obj_fxn.m` - A function that computes the squared error between real and simulated staging curves given the current choice of parameter values
* `pfalc_idc_obj_fxn_constraints.m` - A function that defines constraints on the relationships between parameters over which the objective function is defined.  In particular, this function insists that the transition between the ring and troph stage happens before the transition between the troph and schiz stage
* `pfalc_idc_optimal_params.m` - A function that estimates the global optimal choice of model parameters to fit simulated staging data from a population of independent phase oscillators to experimental staging data
* `pfalc_idc_replication_analysis.m` - A script to quantifiy and visualize the impact of parasite replication on staging curves using the continuous phase-oscillator model
* `pfalc_idc_simulation.m` - A function that performs a numerical simulation of a population of independent generic phase oscillators representing the phases of members of a population of P. falciparum parasites during the intraerythrocytic developmental cycle
* `replicate_oscillators.m` - A function that performs replication of a population of phase oscillators. Replication occurs after each complete cycle (i.e. when a mother oscillator crosses the phase 0=1 after a complete cycle) at which point args.rep_num daughter oscillators are created and initialized with the same phase as their mother and phase velocities drawn from a specified distribution

## data/leise_2012/

* `leise_2012.csv` - Fibroblast PER2::LUC bioluminescence data from Leise et al, 2012 (https://doi.org/10.1371/journal.pone.0033334)

## data/pfalc/

* `opt_params_struct.mat` - Output of `optimal_control_params_table.m` containing optimal parameters of phase oscillator model fit to staging data
* `opt_params_table.csv` -  Output of `generate_tables.m` containing the data in `opt_params_struct.mat` in human-readbale format
* `cont_params_struct.mat` - Output of `create_control_params_table.m` containing optimal parameters under the assumption of a non-zero minimum value of phase oscillator population period variability
* `cont_params_table.csv` - Output of `generate_tables.m` containing the data in `cont_params_struct.mat` in human-readbale format
* `3D7_sequenced_samples_staging.txt` - Staging curves of 3D7 strain
* `D6_sequenced_samples_staging.txt` - Staging curves of D6 strain
* `FVO_sequenced_samples_staging.txt` - Staging curves of FVO strain
* `SA250_sequenced_samples_staging.txt` - Staging curves of SA250 strain
* `HB3_sequenced_samples_staging.txt` - Staging curves for HB3 strain, estimated from Bozdech et al, 2003 (https://doi.org/10.1371/journal.pbio.0000005)


