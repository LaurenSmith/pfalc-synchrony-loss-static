function [p2p_times, peaks, spur_peaks] = compute_p2p_times(time, sig, num_bndry, thresh)
% compute_p2p_times() finds the peaks (local maxima) of the signal sig, 
% eliminates spurious peaks and returns the peak-to-peak times of the 
% filtered set of peaks.
% 
% A peak is called spurious if the difference between its value and either 
% the preceeding or succeeding local min is less than thresh. 
%
% The peak-to-peak time between between the preceeding and succeeding
% maxima of a spurious maximum is ignored so as to not introduce
% inordinately large peak-to-peak times
% - input -
% time - length-T list of timepoints 
% sig - length-T list of values 
% num_bndry - integer >= 0 specifying number of peaks to be omitted to 
%             avoid boundary effects
% thresh - float >= 0 specifying 
%
% - output -
% p2p_times - duration between adjacent, non-spurious, interior peaks
% peaks - array of indices of sig corresponding to interior peaks
% spur_peaks - array of indices of sig corresponding to spurious peaks

% find local maxima and minima
[sigmax, imax, sigmin, imin] = extrema(sig);

% sort extrema in time
[~, idxmax] = sort(time(imax));
sigmax = sigmax(idxmax);
imax = imax(idxmax);
[~, idxmin] = sort(time(imin));
sigmin = sigmin(idxmin);
imin = imin(idxmin);

% remove boundary peaks
nmax = length(imax);
sigmax = sigmax(num_bndry+1:nmax-num_bndry);
imax = imax(num_bndry+1:nmax-num_bndry);

% ensure that each max has a preceeding and succeeding min
if ismember(1, imin)
    % start value is local min, remove bndry mins
    imin(1:num_bndry) = [];
    sigmin(1:num_bndry) = [];
else
    % start value is a local max, remove bndry-1 mins
    imin(1:num_bndry-1) = [];
    sigmin(1:num_bndry-1) = [];
end

if ismember(length(sig), imin)
    % end value is a local min, remove bndry mins
    imin(end-num_bndry+1:end) = [];
    sigmin(end-num_bndry+1:end) = [];
else 
    % end value is a local max, remove bndry-1 mins
    imin(end-num_bndry:end) = [];
    sigmin(end-num_bndry:end) = [];
end

% identify spurious peaks
spur_peaks = zeros(1, length(imax));
for i=1:length(imax)
    min_times = time(imin);
    % find the signal value at the preceeding min
    prec_min_time = max(min_times(time(imin) < time(imax(i))));
    if isempty(prec_min_time)
        prec_min_val = -inf;
    else
        prec_min_val = sigmin(min_times == prec_min_time);
    end
     % find the signal value at the succeeding min
    succ_min_time = min(min_times(time(imin) > time(imax(i))));
    if isempty(succ_min_time)
        succ_min_val = -inf;
    else
        succ_min_val = sigmin(min_times == succ_min_time);
    end
    
    % label the peak as spurious if 'amplitude' is below thresh
    if min(sigmax(i) - prec_min_val, sigmax(i) - succ_min_val) < thresh
        spur_peaks(i) =  1;
    end
end

% decompose peaks into contiguous intervals separated by spurious maxima
% compute peak-to-peak times on contiguous intervals
spur_peaks_idx = [0, find(spur_peaks), length(imax)+1];
p2p_times = [];
for i = 1:length(spur_peaks_idx)-1
    p2p_times = [p2p_times; diff(time(imax(spur_peaks_idx(i)+1:spur_peaks_idx(i+1)-1)))];
end

peaks = imax;
spur_peaks = imax(spur_peaks_idx(2:end-1));

end

