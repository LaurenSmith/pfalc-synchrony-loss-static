function [opt_param, se_at_opt_param, exit_flag, analytic_period_cov, leise_data_p2p_time_cov, leise_data_p2p_sigma] = pfalc_idc_optimal_params(args)
% pfalc_idc_optimal_params() estimates the global optimal choice of model 
% parameters to fit simulated staging data from a population of independent 
% phase oscillators to experimental staging data.
%
% The parameter space is defined by variables
%    init_phase_mean - initial population mean phase (mu_0)
%    init_phase_sigma - initial population phase shape parameter (sigma_0)
%    phase_vel_sigma - population phase velocity shape parameter (sigma)
%    ring_troph_trans - ring to trophozoite stage transition phase
%                      (theta_{r,t})
%    troph_schiz_trans - trophozoite to schizont stage transition phase
%                      (theta_{t,s})
%    mean_cycle_length = mean cycle length in hours
%
% - input -
% args: structure of arguments controlling the model
%
% - output - 
% opt_param: estimate of global optimal choice of parameters to fit model
%            to data
% se_at_opt_param: squared error at optimal parameters
% exit_flag: flag encoding the reason for termination of the global
%            optimization
% analytic_period_cov: coefficient of variation at optimal phase_vel_sigma
%                      (this is also, by construction, the standard 
%                       deviation of the distribution of
%                       phase velocities at the optimal parameters)
% leise_data_p2p_time_cov: coefficient of variation of peak-to-peak times
%                          in the Leise circadian data
% leise_data_p2p_sigma: phase_vel_sigma corresponding to 
%                       leise_data_p2p_time_cov
                 
% extract lower and upper bounds of parameter ranges
lb = [args.init_phase_mean_lb,...
      args.init_phase_sigma_lb,...
      args.phase_vel_sigma_lb, ...
      args.ring_troph_trans_lb,...
      args.troph_schiz_trans_lb,...
      args.mean_cycle_length_lb];
ub = [args.init_phase_mean_ub,...
      args.init_phase_sigma_ub,...
      args.phase_vel_sigma_ub, ...
      args.ring_troph_trans_ub,...
      args.troph_schiz_trans_ub,...
      args.mean_cycle_length_ub];

% set genetic algorithm solver options
opts = optimoptions(@ga,...
                    'PlotFcn',{@gaplotbestf,@gaplotstopping},...
                    'PopulationSize', args.ga_opts_pop_size,... 
                    'MaxGenerations', args.ga_opts_max_gens,...
                    'CrossoverFraction', args.ga_opts_xover_frac,...
                    'MaxStallGenerations', args.ga_opts_max_stall_gens,...
                    'Display', args.ga_opts_disp,...
                    'UseParallel', args.ga_opts_para,...
                    'UseVectorized', args.ga_opts_vect,...
                    'FunctionTolerance', args.ga_opts_fxn_tol,...
                    'HybridFcn', args.ga_opts_hybrid);
                
% run genetic algorithm optimization with the specified arguments
[opt_param, se_at_opt_param, exit_flag, ~] = ga(@(params) args.obj_fxn(params, args),...
                                                args.nvars,...
                                                [], [], [], [],...
                                                lb, ub,...
                                                @(params) args.obj_fxn_const(params),...
                                                opts);
                                            
best_mu = log(pi*opt_param(3)/sin(pi*opt_param(3)));
best_sigma = opt_param(3);

% compute statistics at optimum
analytic_period_mean = 1; % by construction
analytic_period_std = sqrt(exp(-2*best_mu)*(2*pi*best_sigma/sin(2*pi*best_sigma) - (pi*best_sigma/sin(pi*best_sigma))^2));
analytic_period_cov = sqrt(tan(pi*best_sigma)/(pi*best_sigma)-1);

% compute the sigma corresponding to leise_2012 cov
leise_data_p2p_time_cov = 0.084529217253647;
syms s
leise_data_p2p_sigma = double(vpasolve(tan(pi*s)/(pi*s) == leise_data_p2p_time_cov^2+1, s, [0 1/2]));
