function [se] = pfalc_idc_obj_fxn(params, args)
% p_falc_idc_obj_fxn() computes the squared error between real and
% simulated staging curves given the current choice of parameter values
% - input -
% params: vector of parameter values
%          params(1) = initial population mean phase (mu_0)
%          params(2) = initial population phase shape parameter (sigma_0)
%          params(3) = population phase velocity shape parameter (sigma)
%          params(4) = ring to trophozoite stage transition phase
%                      (theta_{r,t})
%          params(5) = trophozoite to schizont stage transition phase 
%                      (theta_{t,s})
%          params(6) = mean cycle length in hours
% args: structure of arguments and data needed to specify the objective
%       function
%
% - output - 
% se: total squared error between real and simulated staging curves

% % rather than introduce a linear constraint to the objective function, 
% % we can penalize infeasible parameter choices for which the 
% % trophozoite-to-schizont transition occurs before the 
% % ring-to-trophozoite transition. This optimum found by GA is insensitive
% % to this methodology. 
% if params(4) > params(5)
%     se=100000;
%     return
% end

[staging_sim, ~] = pfalc_idc_simulation(params, args);

se = sum(sum((staging_sim - args.staging_data).^2));

end

